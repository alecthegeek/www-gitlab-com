---
layout: markdown_page
title: "Multicloud deployment for GitOps using GitLab: A demo"
description: "How multicloud compatibility supports GitOps workflows."
---
 
GitOps workflows use a Git repository as a single source of truth to enable collaboration, bringing infrastructure teams together to accelerate software development and delivery. When operations teams use GitOps workflows, there are benefits beyond version control when using GitLab as the core repository. Teams use GitLab for its collaborative platform, ease of infrastructure deployments, and multicloud compatibility.

# Multicloud deployments using GitOps and GitLab

This demo shows how to deploy applications to three Kubernetes servers using a common workflow. Teams will learn how to successfully deploy applications using Auto DevOps, powered by GitLab CI, with Helm and Kubernetes.
 
 <!-- blank line -->
<figure class="video_container">
  <iframe src="https://www.youtube-nocookie.com/embed/heQ1WY_08Tc" frameborder="0" allowfullscreen="true"> </iframe>
</figure>
<!-- blank line -->
 
The first step is to open the [gitops-demo group README.md file](https://gitlab.com/gitops-demo/readme), which shows the structure of the gitops-demo group. There are a few projects and two subgroups: infrastructure and [applications](https://gitlab.com/gitops-demo/apps). 
 
![GitOps demo map](/images/blogimages/gitops_series_2019/gitops_demo.png){: .shadow.center.medium}

# Inside the applications folder
 
There are four applications for this demo: my-asp-net-app1; my-spring-app2; my-ruby-app3; my-python-app4, and three Kubernetes clusters, with each corresponding to a different cloud environment: Microsoft Azure (AKS), Amazon (EKS), and Google Cloud (GKE).
 
Clicking the Kubernetes button on the left-hand corner reveals that all of the clusters are registered to GitLab. The environmental scopes represent which application is deployed to each cloud.

## ASP.NET application on AKS

### AutoDevOps at work
 
[The first example](https://gitlab.com/gitops-demo/apps/my-asp-net-app1) is an ASP.NET application, which is the equivalent of a Hello, World app. There are a few modifications that are specific to how this application is deployed, which lives in the [application CI file](https://gitlab.com/gitops-demo/apps/my-asp-net-app1/blob/master/.gitlab-ci.yml).
 
The first step is to import the main Auto DevOps template by setting a couple of variables. Then, it’s important to override a few commands for stages that are more applicable to .net code, and finally, set the environment automatically to deploy production into AKS.
 
```yaml
include:
  - template: Auto-DevOps.gitlab-ci.yml
 
variables:
  DEPENDENCY_SCANNING_DISABLED: "true"
 
test:
  stage: test
  image: microsoft/dotnet:latest
  script:
    - 'dotnet test --no-restore'
 
license_management:
  stage: test
  before_script:
    - sudo apt-get update
    - sudo apt-get install -y dotnet-runtime-2.2 dotnet-sdk-2.2
 
production:
  environment:
    name: aks/production
    url: http://$CI_PROJECT_PATH_SLUG.$KUBE_INGRESS_BASE_DOMAIN
```
{: .language-yaml}
 
The pipeline will run automatically and deploy successfully. By viewing [the pipeline](https://gitlab.com/gitops-demo/apps/my-asp-net-app1/pipelines/88314435), it’s possible to see how deployment works.
 
![GitOps demo map](/images/blogimages/gitops_series_2019/app1_pipeline.jpg){: .shadow.center.medium}
 
The stages of the pipeline from build to production for the ASP.NET application.
{: .note.text-center}
 
A quick look inside the pipeline shows that all the jobs passed successfully. The Auto DevOps feature kicked off the build stage, which creates a [Docker](/blog/2020/11/18/docker-hub-rate-limit-monitoring/) container and uploads it to the built-in Docker registry. The test phase is comprehensive and includes [container scanning](/blog/2020/07/28/container-security-in-gitlab/), license management, SAST, and unit tests. To dive deeper into the testing results, click the security and license tabs. The application deploys to production in the final stage of the pipeline.

### Inside the AKS cluster
 
The ASP.NET application is deploying to the AKS cluster. Go to Operations > Environments to see the environment configured for this application. Metrics such as the HTTP error rates, latency rates, and throughput are available, because [Prometheus](/blog/2019/07/23/anomaly-detection-using-prometheus/) is already integrated into GitLab’s Kubernetes clusters.
 
The environment can be launched directly by clicking the live URL to see the application running on AKS. There isn’t a lot of extra code beyond what is already configured into GitLab that tells the application how to deploy. The Auto DevOps feature creates a Helm chart and deploys it to Kubernetes and AKS.
 
## Java Spring application on GKE
 
In the demo, you’ll learn how to configure the [Spring application](https://gitlab.com/gitops-demo/apps/my-spring-app2) similarly as the ASP.NET application by using a Dockerfile. The [Dockerfile](https://gitlab.com/gitops-demo/apps/my-spring-app2/blob/master/Dockerfile) is placed in the repository root directory.
 
```docker
ROM maven:3-jdk-8-alpine
WORKDIR /usr/src/app
COPY . /usr/src/app
RUN mvn package
ENV PORT 5000
EXPOSE $PORT
CMD [ "sh", "-c", "mvn -Dserver.port=${PORT} spring-boot:run" ]
```
 
The Spring application deployment differs from the ASP.NET application in one way: It does not need any overrides to the AutoDevOps template, because it uses the default template, deploying to GKE instead of AKS. The workflow for application deployment is identical regardless of what cloud the application is being deployed to. This makes [multicloud](/blog/2019/11/06/gitlab-ci-cd-is-for-multi-cloud/) easy.
 
It’s important to produce a similar build, test, and production run in this environment. By taking this step, teams can obtain the same metrics, the error rates, latencies, and throughputs. In this case, the application is running automatically in a container on Kubernetes in the GKE cluster.
 
## Python application on EKS
 
The final example is a [Python application](https://gitlab.com/gitops-demo/apps/my-python-app4) that deploys on EKS. The components are similar to the previous examples, and use [gitlab-ci.yml to change the production environment to EKS](https://gitlab.com/gitops-demo/apps/my-python-app4/blob/master/.gitlab-ci.yml), and a Dockerfile to prepare the Helm chart. There are also a few overrides.
 
```yaml
include:
  - template: Auto-DevOps.gitlab-ci.yml
test:
  image: python:3.7
  script:
    - pip install -r requirements.txt
    - pip install pylint
    - pylint main.py
production:
  environment:
    name: eks/production
    url: http://$CI_PROJECT_PATH_SLUG.$KUBE_INGRESS_BASE_DOMAIN
```
 
The GitLab CI file tells the application to deploy on EKS.
{: .note.text-center}
 
```docker
FROM python:3.7
WORKDIR /app
ADD . /app/
RUN pip install -r requirements.txt
EXPOSE 5000
CMD ["python", "/app/main.py"
```
 
The [Dockerfile](https://gitlab.com/gitops-demo/apps/my-python-app4/blob/master/Dockerfile) gets the Helm chart ready.
{: .note.text-center}
 
Just like in previous examples, the [pipeline](https://gitlab.com/gitops-demo/apps/my-python-app4/pipelines/88314654) runs the same way as in the other applications with build, test, and production phases. Once the application is deployed to EKS, you can open up the live link and see the Python application in your browser window.
 
GitLab is a true multicloud solution that enables businesses to make decisions about which cloud provider they want to use, without disparate workflows, while still maintaining good GitOps practices. All of this is a consistent interface with the same workflow, making it simple to deploy to any major cloud running Kubernetes integrated with GitLab.
 
# GitLab for GitOps
 
A good GitOps practice involves making a Git repository the single source of truth for all code. While any Git repository could suffice for good GitOps procedure, there are few DevOps tools that truly encompass the core pillars of GitOps: collaboration, transparency in process, and [version control](/blog/2020/11/12/migrating-your-version-control-to-git/).
 
Tools like [epics](/blog/2020/01/21/epics-three-features-accelerate-your-workflow/), issues, and [merge requests](/blog/2020/12/14/merge-trains-explained/), which are the crux of GitLab, foster communication and transparency between teams. Infrastructure teams can build code using Terraform or [Ansible templates](/blog/2019/07/01/using-ansible-and-gitlab-as-infrastructure-for-code/) in GitLab, and deploy to the cloud using GitLab CI. GitLab is the true multicloud solution, allowing teams to deploy an application to any cloud service using GitLab CI and Kubernetes without having to significantly augment their workflows.

# Ready to learn more about GitOps?

* [See how GitLab strengthens GitOps workflows](/solutions/gitops/)
* [Understand the future of GitOps from industry leaders](/why/gitops-infrastructure-automation/)
* [Download the beginner’s guide to GitOps](https://page.gitlab.com/resources-ebook-beginner-guide-gitops.html)
